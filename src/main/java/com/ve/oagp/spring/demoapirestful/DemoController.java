package com.ve.oagp.spring.demoapirestful;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author Oswaldo
 */
@RestController
public class DemoController {
    private static final String TEMPLATE = "Hola, %s!";

    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    public Message hello(@RequestBody User user) {
        return new Message(String.format(TEMPLATE, user.getName()));
    }
    
    @RequestMapping(value = "/ready", method = RequestMethod.GET)
    public Message ready() {
        return new Message("OK");
    }
    
}
