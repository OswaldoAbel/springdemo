package com.ve.oagp.spring.demoapirestful;

/**
 *
 * @author oswaldo
 */
public class Message {
    private String message;

    public Message() {
    }

    public Message(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    
    
}
